from restdemo.models import *
from faker import Faker
from pprint import pprint
from random import randint

fake = Faker()

models = 	[	{ 	
					"model_name": "Questions", 
					"fields": 	[	
									{"name": "title", "type": "name"},
									{"name": "private", "type": "boolean"}, 
									{"name": "user_id", "type": "id"},
								] 
				},
				
				{ 
					"model_name": "Answers", 
					"fields": 	[		
									{"name": "body", "type": "text"},
									{"name": "question_id", "type": "id"},
									{"name": "user_id", "type": "id"},
								]
				},	

				{ 
					"model_name": "Tenants", 
					"fields": 	[		
									{"name": "name", "type": "name"},
									{"name": "api_key", "type": "UUID4"},
								]
				},	

				{ 
					"model_name": "User", 
					"fields": 	[		
									{"name": "name", "type": "name"},
								]
				}	


			]


constant_no_of_items_per_field = 10


def _generate_data(models):

	no_of_models = len(models)

	for i in range(0, no_of_models):

		model = models[i]
		fields = models[i]["fields"]
		
		for j in range(0, len(fields)):
			fields[j]["value"] = []
		
			for k in range(0, constant_no_of_items_per_field):
				
				if fields[j]["type"] == "name":
					name = fake.name()
					fields[j]["value"].append(name)

				if fields[j]["type"] == "boolean":
					boolean = fake.boolean()
					fields[j]["value"].append(boolean)

				if fields[j]["type"] == "UUID4":
					uuid4 = fake.uuid4()
					fields[j]["value"].append(uuid4)

				# if fields[j]["type"] == "id":
				# 	id = randint(1, constant_no_of_items_per_field)
				# 	fields[j]["value"].append(id)

				if fields[j]["type"] == "text":
					text = "Some random text"
					fields[j]["value"].append(text)

	return models


def _modify_db(models):

	# print("Before")
	# pprint(models)

	data = _generate_data(models)

	# print()
	# print("After")
	# pprint(data)
	# print()

	# for models in data:
	# 	model = models["model_name"]
	# 	fields = models["fields"]


	model = data[3]["model_name"]
	fields = data[3]["fields"]

	if model == "User":

		for name in fields[0]["value"]:
			# print(name)
			user = RestUserModel(name=name)
			user.save()

	model = data[2]["model_name"]
	fields = data[2]["fields"]


	if model == "Tenants":
		name = fields[0]["value"]
		api_keys = fields[1]["value"]
		
		for i in range(0, constant_no_of_items_per_field):
			tenant = TenantModel(name=name[i], api_key=api_keys[i])
			tenant.save()




	model = data[0]["model_name"]
	fields = data[0]["fields"]


	if model == "Questions":
		title = fields[0]["value"]
		private = fields[1]["value"]

		for i in range(0, constant_no_of_items_per_field):
			random_index = randint(0, constant_no_of_items_per_field - 1)
			user_id = RestUserModel.objects.all()[random_index]

			question = QuestionModel(title=title[i], private=private[i], user_id=user_id)
			question.save()



	model = data[1]["model_name"]
	fields = data[1]["fields"]


	if model == "Answers":
		body = fields[0]["value"]
		# question_id = fields[1]["value"]
		# user_id = fields[2]["value"]

		for i in range(0, constant_no_of_items_per_field):
			random_index = randint(0, constant_no_of_items_per_field - 1)
			question_id = QuestionModel.objects.all()[random_index]

			random_index = randint(0, constant_no_of_items_per_field - 1)
			user_id = RestUserModel.objects.all()[random_index]

			answer = AnswerModel(body=body[i], question_id=question_id, user_id=user_id)
			answer.save()



_modify_db(models)

print()
print("Fake data generated.")
print()

