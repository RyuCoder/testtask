# 
# Front end have to be setup with a custom request header using the following code.
# api_key will be different for different tenants
#
# $.ajaxSetup({
#     headers: { "api_key": "ed77d5b85c8554dc561af22e3ff1d83d" }
# }); 
# 


from restdemo import models
from django.utils import timezone
from django.http import JsonResponse

import uuid, datetime, pytz


constant_daily_request_limit = 100


class ApiKeyRequiredMiddleware(object):

	def __init__(self, get_response):
		self.get_response = get_response

	def __call__(self, request):


		# Django calls the process_view function before the function is called
		# self.process_view(request)

		response = self.get_response(request)
		response['API_KEY']=""
		
		#Any code that needs to be run after the function should be written here
		#process_view_after() is not a standard django function
		self.process_view_after(request, response)

		return response
	

	def process_view(self, request, view_function, view_args, view_kwargs):
		# Middleware is gonna be devided in 3 diff parts
		# All parts should have a separate function ideally
		# 
		# Part I - 
		# Read api_key from the request object headers
		# Check if the api_key is in the list of tenant api_keys
		# 	if not present 
		# 		return jsonresponse of bad api key
		# 	otherwise continue
				
		# Part I -
		# Wrong API Key for testing
		# api_key = "ed77d5b4-5c85-54dc-561a-f22e3ff1d83d"
		
		# Valid API Key for testing 
		# This key is enabled so that the demo will work nicely
		# If the user sends a different api key, it will get updated in the next block of code
		api_key = "ed77d5b8-5c85-54dc-561a-f22e3ff1d83d"

		if request.is_ajax():
			api_key = request.META.get('HTTP_API_KEY')

		# converting the api_key from string object to UUID object
		api_key = uuid.UUID(api_key)

		api_keys = models.TenantModel.objects.values('api_key')

		match_found = False
				
		for item in api_keys:
			if api_key == item['api_key']:
				match_found = True

		if match_found == False:
			return JsonResponse({"status": "Not Allowed", "error": "Bad API Key provided."})
 

		# Part II -
		# Update the ApiKeyCount model on every request
		tenant = models.TenantModel.objects.filter(api_key=api_key)[0]

		api_requests = models.ApiKeyCount.objects.create(tenant_id=tenant)
		api_requests.save()


		# Part III -
		# Add a piece of middleware to throttle API requests on a per-Tenant basis. 
		# After the first 100 requests per day, throttle to 1 request per 10 seconds.

		count_daily = 0
		count_last_ten_seconds = 0


		# get count for tenant api for todays requests
		today = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
		now = timezone.now()
		last_ten_seconds = now - datetime.timedelta(seconds=10)
		# get the count of no of requests mad ein the last 10 seconds

		count_daily = models.ApiKeyCount.objects.filter(tenant_id=tenant.id).filter(request_time__date=today).count()
		count_last_ten_seconds = models.ApiKeyCount.objects.filter(tenant_id=tenant.id).filter(request_time__gt=last_ten_seconds).count()

		last_item = models.ApiKeyCount.objects.filter(tenant_id=tenant.id).order_by("request_time").last()
		last_request_time = last_item.request_time

		if now.date() == last_request_time.date():
			if count_daily > constant_daily_request_limit:
				if count_last_ten_seconds > 1:
					return JsonResponse({"status": "Not Allowed", "error": "Daily request limit exceeded."})


	def process_view_after(self, request, response):
		pass

