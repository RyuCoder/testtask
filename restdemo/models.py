from django.db import models
import uuid

class QuestionModel(models.Model):
    title = models.CharField(max_length=256)
    private = models.BooleanField(default=False)
    user_id = models.ForeignKey('RestUserModel', on_delete=models.CASCADE)

    def __str__(self):
    	return self.title


class AnswerModel(models.Model):
    body = models.TextField(max_length=1024)
    question_id = models.ForeignKey('QuestionModel', on_delete=models.CASCADE)
    user_id = models.ForeignKey('RestUserModel', on_delete=models.CASCADE)

    def __str__(self):
        return self.body


class TenantModel(models.Model):
    name = models.CharField(max_length=256)
    api_key = models.UUIDField(default=uuid.uuid4, unique=True)

    def __str__(self):
        return self.name


class RestUserModel(models.Model):
    name = models.CharField(max_length=256)
    
    def __str__(self):
        return self.name


class ApiKeyCount(models.Model):
    tenant_id = models.ForeignKey('TenantModel', on_delete=models.CASCADE)
    request_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.tenant_id

