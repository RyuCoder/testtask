from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .models import QuestionModel, AnswerModel, TenantModel, RestUserModel
import pdb


def dashboard(request):
	questions = QuestionModel.objects.all().count()
	answers = AnswerModel.objects.all().count()
	tenants = TenantModel.objects.all().count()
	users = RestUserModel.objects.all().count()

	context = 	{
					"questions": questions,
					"answers": answers,
					"tenants": tenants,
					"users": users,
				}

	return render(request, "dashboard.html", context)


def questions(request):
	pdb.set_trace()

	questions = QuestionModel.objects.filter(private=False)
	answers = AnswerModel.objects.all()
	tenants = TenantModel.objects.all()
	users = RestUserModel.objects.all()

	data = []

	for question in questions:

		for answer in answers.filter(question_id=question.id):

			# For every new answer a new entry of item is gonna be created.
			# That is not very good as all the answers can be dumped into one question entry 
			# Rewrite the code to dump all answers in one single question entry.

			item = {

						"question_id": question.id,

						"title": question.title,

						"private": "false", 

						"user_id": question.user_id.__dict__["id"],

						"answer":{

									"body": answer.body,

									"user_id": answer.user_id.__dict__["id"]
								}

					}


			data.append(item)

	context = 	{
					"data": data
				}

	return JsonResponse(context)

